#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface StorageManager : NSObject {
  NSPersistentContainer *storage;
}

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;

+ (id)sharedManager;

@end
