#import "AppDelegate.h"
#import "NativeBridge.h"
#import "StorageManager.h"
#import "SessionManager.h"
#import "Firebase.h"

@import Sentry;

@implementation NativeBridge

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(getFirebaseToken:(RCTResponseSenderBlock)callback) {
  callback(@[[FIRMessaging messaging].FCMToken]);
}

RCT_EXPORT_METHOD(setSession:(NSString *)sessionString) {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setObject:sessionString forKey:@"adrich.sessionId"];
  [defaults synchronize];
  [(AppDelegate*)[[UIApplication sharedApplication]delegate]getInitialToken];
}

RCT_EXPORT_METHOD(setEmail:(NSString *)email) {
  SentryUser *user = [[SentryUser alloc] init];
  user.email = email;
  [SentryClient.sharedClient setUser:user];
}

RCT_EXPORT_METHOD(setIgnoreMeasurement) {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setBool:YES forKey:@"adrich.ignoreMeasurement"];
  [defaults synchronize];
  [(AppDelegate*)[[UIApplication sharedApplication]delegate]getInitialToken];
}

RCT_EXPORT_METHOD(unsetIgnoreMeasurement) {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setBool:NO forKey:@"adrich.ignoreMeasurement"];
  [defaults synchronize];
  [(AppDelegate*)[[UIApplication sharedApplication]delegate]getInitialToken];
}

RCT_EXPORT_METHOD(clearSession) {
  [[SessionManager sharedManager] setSessionString:nil];
}

RCT_EXPORT_METHOD(getMeasurements:(RCTResponseSenderBlock)callback) {
  
  NSManagedObjectContext *managedContext = [[[StorageManager sharedManager] persistentContainer] viewContext];

  NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Measurement"];
  
  NSError *error = nil;
  NSArray *results = [managedContext executeFetchRequest:request error:&error];
  if (!results) {
    NSLog(@"Error fetching Measurement objects: %@\n%@", [error localizedDescription], [error userInfo]);
    abort();
  }
  
  NSMutableArray *dictionaryArray = [[NSMutableArray alloc] init];
  for (NSManagedObject *obj in results) {
    NSArray *keys = [[[obj entity] attributesByName] allKeys];
    NSDictionary *dictionary = [obj dictionaryWithValuesForKeys:keys];
    [dictionaryArray addObject:dictionary];
  }
  
  NSData *nsData = [NSJSONSerialization dataWithJSONObject:dictionaryArray options:NSJSONWritingPrettyPrinted error:&error];
  NSString *jsonString = [[NSString alloc] initWithData:nsData encoding:NSUTF8StringEncoding];
  
  callback(@[jsonString]);
}

@end
