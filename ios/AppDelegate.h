#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import <UserNotifications/UserNotifications.h>

#import "Util.h"
#import "NativeBridge.h"
#import "ErrorStrings.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

- (void) getInitialToken;

@property (nonatomic, strong) UIWindow *window;

@property (nonatomic, strong) NSArray *services;
@property (nonatomic, strong) NSArray *characteristics;

@property (nonatomic, strong) CBCentralManager *centralManager;
@property (nonatomic, strong) CBPeripheral *peripheral;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *lastKnownLocation;

@property (nonatomic, strong) Util *util;
@property (nonatomic, strong) NSString *phase;
@property (nonatomic, strong) NSString *ble_status;
@property (nonatomic, strong) NSString *location_status;

@property (nonatomic, strong) UNUserNotificationCenter *notificationCenter;

@end
