#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

#import "Util.h"

@interface EventManager : RCTEventEmitter <RCTBridgeModule>

@property (nonatomic, strong) EventManager *sharedInstance;
@property (nonatomic, strong) Util *util;

- (void)pushNotificationReceivedWithTitle:(NSString*)title andBody:(NSString*)body;
- (void)payloadLifecycleEventWithName:(NSString*)name;
- (void)errorReceived:(NSString *)error;

@end
