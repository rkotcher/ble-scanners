//
//  Util.m
//  adrichmobile

#import "Util.h"

#import "ReactNativeConfig.h"
#import "SessionManager.h"

#import <Foundation/Foundation.h>

@implementation Util

- (void) sendPostRequest : (NSString *)endpoint andData:(NSData *)postData andHandler:(CompletionHandler) handler {
  NSString *baseUrl = [ReactNativeConfig envFor:@"API_SERVER"];
  NSString *fullUrl = [baseUrl stringByAppendingString:endpoint];
  
  NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
  [request setHTTPMethod:@"POST"];
  [request setURL:[NSURL URLWithString:fullUrl]];
  [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  [request setHTTPBody:postData];
  
  NSString *sessionHeader = @"adrich.session=";
  
  /* add session if it exists */
  if ([[NSUserDefaults standardUserDefaults] objectForKey:@"adrich.sessionId"] != nil) {
    NSString *sessionCookie = [[NSUserDefaults standardUserDefaults] stringForKey:@"adrich.sessionId"];
    [request addValue:[sessionHeader stringByAppendingString:sessionCookie] forHTTPHeaderField:@"Cookie"];
  }
  
  [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]
                         completionHandler:handler];
  
}
@end
