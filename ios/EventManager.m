#import "EventManager.h"

@implementation EventManager

RCT_EXPORT_MODULE();

+ (id)allocWithZone:(NSZone *)zone {
  static EventManager *sharedInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{\
    sharedInstance = [super allocWithZone:zone];
  });
  return sharedInstance;
}

- (NSArray<NSString *> *)supportedEvents
{
  return @[
    @"PushNotificationReceived",
    @"PayloadLifecycleEventOccurred",
    @"ErrorReceived"
  ];
}

- (void)pushNotificationReceivedWithTitle:(NSString*)title andBody:(NSString*)body {
  [self sendEventWithName:@"PushNotificationReceived" body:@{@"title": title, @"body": body}];
}

- (void)payloadLifecycleEventWithName:(NSString*)name {
  /*
   *  ADRICH_PERIPHERAL_DISCOVERED
   *  ADRICH_PERIPHERAL_CONNECTED
   *  ADRICH_PAYLOAD_TRANSMITTED
   *  ADRICH_PAYLOAD_RESOLVED
   */
  [self sendEventWithName:@"PayloadLifecycleEventOccurred" body:@{ @"name": name }];
}

- (void)errorReceived:(NSString *)error
{
  self.util = [[Util alloc] init];
  
  NSString *device = @"iOS ";
  NSString *deviceType = [device stringByAppendingString:[[UIDevice currentDevice] systemVersion]];
  NSDictionary *errorDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                       @"pj@dev.com", @"user",
                       deviceType, @"deviceType",
                       error, @"error",
                             nil];
  
  NSError *err;
  NSData *postData = [NSJSONSerialization dataWithJSONObject:errorDict options:kNilOptions error:&err];
  
  NSLog(@"%@", [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding]);
  [self.util sendPostRequest: @"/api/errors" andData:postData  andHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    if ([httpResponse statusCode] == 200) {
      NSLog(@"Error updated in database");
    }
  }];
  [self sendEventWithName:@"ErrorReceived" body:errorDict];
}
@end
