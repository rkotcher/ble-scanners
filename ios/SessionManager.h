#import <Foundation/Foundation.h>

@interface SessionManager : NSObject {
  NSString *sessionString;
}

@property (nonatomic, strong) NSString *sessionString;

+ (id)sharedManager;

@end
