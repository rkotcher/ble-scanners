#import "StorageManager.h"

@implementation StorageManager

@synthesize persistentContainer;

#pragma mark Singleton Methods

+ (id)sharedManager {
  static StorageManager *sharedMyManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedMyManager = [[self alloc] init];
  });
  return sharedMyManager;
}

- (id)init {
  if (self = [super init]) {
    persistentContainer = [[NSPersistentContainer alloc] initWithName:@"adrichdata"];
    [persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
      if (error != nil) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
      }
    }];
  }
  return self;
}

- (void)dealloc {
  // Should never be called, but just here for clarity really.
}

@end
