//
//  Util.h
//  adrichmobile
//

#import <Foundation/Foundation.h>

typedef void (^CompletionHandler)(NSURLResponse *response, NSData *data, NSError *error);

@interface Util : NSObject

- (void) sendPostRequest: (NSString *)endpoint andData:(NSData *)postData andHandler:(CompletionHandler) handler;

@end
