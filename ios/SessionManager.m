#import "SessionManager.h"

@implementation SessionManager

@synthesize sessionString;

#pragma mark Singleton Methods

+ (id)sharedManager {
  static SessionManager *sharedMyManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedMyManager = [[self alloc] init];
  });
  return sharedMyManager;
}

- (void)dealloc {
  // Should never be called, but just here for clarity really.
}

@end
