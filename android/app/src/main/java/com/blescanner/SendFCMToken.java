package com.adrichmobile;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class SendFCMToken extends Thread {
    private Context context;

    public SendFCMToken(Context context) {
        this.context = context;
    }


    public void run() {
        try {
            SharedPreferences mSettings = context.getSharedPreferences("Session", 0);
            SharedPreferences defaultPrefs = PreferenceManager.getDefaultSharedPreferences(context);

            if (!defaultPrefs.contains("fcm")) {
                return;
            }

            if (!mSettings.contains("id")) {
                return;
            }

            String newToken = defaultPrefs.getString("fcm", null);

            URL url = new URL(BuildConfig.API_SERVER + "/api/firebasetoken");
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            uc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");


            String sessionString= mSettings.getString("id", null);
            String myCookie = "adrich.session=" + sessionString;
            uc.setRequestProperty("Cookie", myCookie);

            uc.setRequestMethod("POST");
            uc.setDoInput(true);
            uc.setInstanceFollowRedirects(false);
            uc.connect();

            FCMToken fcm = new FCMToken(newToken);
            Gson gson = new Gson();
            String tokenAsJson = gson.toJson(fcm);

            OutputStreamWriter writer = new OutputStreamWriter(uc.getOutputStream(), "UTF-8");
            writer.write(tokenAsJson);
            writer.close();

            if (uc.getResponseCode() == 200) {
                SharedPreferences.Editor editor = defaultPrefs.edit();
                editor.remove("fcm");
                editor.commit();
            }
            uc.disconnect();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
