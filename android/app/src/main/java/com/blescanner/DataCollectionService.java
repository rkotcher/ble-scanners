/*
 *  Author: Robert Kotcher
 *  Email: rkotcher@gmail.com
 *
 *  MIT License
 */

package com.adrichmobile;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.sentry.Sentry;

import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;

@TargetApi(Build.VERSION_CODES.M)
public class DataCollectionService extends Service {
    public static boolean isActive = false;

    public static String DATA_PIPELINE_PROGRESS = null;

    private String PAYLOAD_SERVICE_UUID = "0000a1b0-0000-1000-8000-00805f9b34fb";
    private String PAYLOAD_CHARACTERISTIC_UUID = "00000321-0000-1000-8000-00805f9b34fb";

    private long LOCATION_UPDATE_INTERVAL = 1800000;  /* 30 minutes */
    private long LOCATION_FASTEST_INTERVAL = 600000; /* 10 minutes */

    private Context context;

    private final int LABEL_TRANSMIT_TIME_MS = 10000;
    private final int SCANNER_DOWN_TIME = 2000;
    private final int SCANNER_LIFETIME_MS = 10000;

    private Handler bootScanHandler = null;
    private Handler startScanHandler = null;
    private Handler restartScanHandler = null;
    private Handler attemptConnectHandler = null;
    private Handler attemptDisconnectHandler = null;

    private Runnable bootScan = null;
    private Runnable startScan = null;
    private Runnable restartScan = null;
    private Runnable attemptConnect = null;
    private Runnable attemptDisconnect = null;

    private LocationRequest mLocationRequest;
    private Location lastKnownLocation;

    private BluetoothManager bluetoothManager;
    private BluetoothAdapter bluetoothAdapter;

    private Map<Integer, Boolean> scanErrorsReceived = new HashMap<Integer, Boolean>();

    @Override
    public void onCreate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String NOTIFICATION_CHANNEL_ID = "adrich.io";
            String channelName = "My scanning service";
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_LOW);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setContentTitle("BleScanner is running.")
                    .setContentText("As you use the product you'll occasionally hear from you favorite brands.")
                    .setPriority(NotificationManager.IMPORTANCE_LOW)
                    .build();
            startForeground(2, notification);
        } else {
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

            notificationBuilder
                    .setContentTitle("BleScanner is running.")
                    .setContentText("As you use the product you'll occasionally hear from you favorite brands.")
                    .setSmallIcon(R.drawable.adrich_android_notify_icon);

            startForeground(1, notificationBuilder.build());
        }
    }

    private void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_FASTEST_INTERVAL);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(getApplicationContext());
        settingsClient.checkLocationSettings(locationSettingsRequest);

        if (
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            Sentry.capture("Location permissions have not been granted by user.");
            return;
        }

        // Try to immediately get the last reading. According to the docs, this should
        // rarely be null.
        LocationServices.getFusedLocationProviderClient(getApplicationContext())
            .getLastLocation()
            .addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    lastKnownLocation = location;
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Sentry.capture(e);
                }
            });

        // Set the location to update every so often. We could probably do a
        // little better with this part.
        LocationServices.getFusedLocationProviderClient(getApplicationContext())
            .requestLocationUpdates(mLocationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    lastKnownLocation = locationResult.getLastLocation();
                }
            },
            Looper.myLooper());
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = this;
        bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

        if (bluetoothAdapter == null) {
            Sentry.capture("Bluetooth adapter was null on scanner boot.");
            stopSelf();
            return START_NOT_STICKY;
        }

        boolean serviceIsNotAttemptingToRestart = (intent != null);

        if (!isActive && serviceIsNotAttemptingToRestart) {
            runnable.run();
            startLocationUpdates();
            isActive = true;
        } else {
            Log.d("@@@@@", "Prevented another runnable from starting.");
        }

        return START_STICKY;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private Runnable runnable = new Runnable() {
        private BluetoothLeScanner scanner;

        final ScanSettings.Builder builder = new ScanSettings.Builder();
        final ScanSettings settings = builder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build();

        public void run() {
            try {
                bootScanner();
            } catch (Exception e) {
                Sentry.capture(e);
            }
        }

        class StartScannerAndScheduleReboot implements Runnable {
            @Override
            public void run() {
                if (bluetoothAdapter.isEnabled()) {
                    scanner = bluetoothAdapter.getBluetoothLeScanner();
                    if (scanner != null) {
                        // List<ScanFilter> scanFilters = new ArrayList<>();
                        // ScanFilter scanFilter = new ScanFilter.Builder()
                        //         .setServiceUuid(ParcelUuid.fromString(PAYLOAD_SERVICE_UUID))
                        //         .build();
                        // scanFilters.add(scanFilter);
                        //
                        // scanner.startScan(scanFilters, settings, sensorReadingHandler);
                        scanner.startScan(null, settings, sensorReadingHandler);
                    }
                } else {
                    bluetoothAdapter.enable();
                }

                bootScanHandler = new Handler(Looper.getMainLooper());
                bootScan = new RebootScanner();
                bootScanHandler.postDelayed(bootScan, SCANNER_LIFETIME_MS);
            }
        }

        class RebootScanner implements Runnable {
            @Override
            public void run() {
                bootScanner();
            }
        }

        private void bootScanner() {
            try {
                scanner.stopScan(sensorReadingHandler);
            } catch (Exception e) {
                /* We can ignore this - there are cases that this will happen & it's ok */
            }

            startScanHandler = new Handler(Looper.getMainLooper());
            startScan = new StartScannerAndScheduleReboot();
            startScanHandler.postDelayed(startScan, SCANNER_DOWN_TIME);
        }

        private ScanCallback sensorReadingHandler = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                try {
                    final BluetoothDevice device = result.getDevice();

                    if (device.getName() != null && device.getName().equals("ADRICH")) {
                        EventEmitter.sendEvent("ADRICH_PERIPHERAL_DISCOVERED");
                        DATA_PIPELINE_PROGRESS = "A peripheral was discovered";

                        // Stop scanning and end all Handlers.
                        scanner.stopScan(sensorReadingHandler);
                        bootScanHandler.removeCallbacksAndMessages(null);
                        startScanHandler.removeCallbacksAndMessages(null);

                        attemptConnectHandler = new Handler(Looper.getMainLooper());
                        attemptConnect = new Runnable() {
                            @Override
                            public void run() {
                                device.connectGatt(context, false, gattCallback);
                            }
                        };
                        attemptConnectHandler.postDelayed(attemptConnect, 80); // suggested 60 - 100 ms

                        restartScanHandler = new Handler(Looper.getMainLooper());
                        restartScan = new Runnable() {
                            @Override
                            public void run() {
                                if (DATA_PIPELINE_PROGRESS != null) {
                                    Sentry.capture(DATA_PIPELINE_PROGRESS);
                                    DATA_PIPELINE_PROGRESS = null;
                                }

                                bootScanner();
                            }
                        };
                        restartScanHandler.postDelayed(restartScan, LABEL_TRANSMIT_TIME_MS);
                    }
                } catch (Exception e) {
                    Sentry.capture(e);
                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                if (!scanErrorsReceived.containsKey(errorCode)) {
                    Sentry.capture(new Exception("Scan failed with error code " + errorCode));
                    scanErrorsReceived.put(errorCode, true);
                }
            }
        };

        private BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(final BluetoothGatt gatt, int status, int newState) {
                if (newState == STATE_CONNECTED) {
                    EventEmitter.sendEvent("ADRICH_PERIPHERAL_CONNECTED");
                    DATA_PIPELINE_PROGRESS = "A peripheral was connected to";
                    gatt.requestMtu(185);

                    // It doesn't really matter how long this takes, but we do eventually want
                    // to ensure that all connections are closed within a reasonable amount of time.
                    attemptDisconnectHandler = new Handler(Looper.getMainLooper());
                    attemptDisconnect = new Runnable() {
                        @Override
                        public void run() {
                        try {
                            // We only need to close the connection. Close disconnects and removes
                            // reference to this device.
                            gatt.close();
                        } catch (Exception e) {
                            /* Ignore this - maybe a device closes connection automatically? */
                        }
                        }
                    };
                    attemptDisconnectHandler.postDelayed(attemptDisconnect, LABEL_TRANSMIT_TIME_MS);
                }
            }

            @Override
            public void onMtuChanged (final BluetoothGatt gatt, int mtu, int status) {
                EventEmitter.sendEvent("ADRICH_MTU_CHANGED");
                DATA_PIPELINE_PROGRESS = "MTU was changed successfully";
                gatt.discoverServices();
            }

            @Override
            public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
                for (BluetoothGattService service : gatt.getServices()) {
                    if (service.getUuid().toString().equals(PAYLOAD_SERVICE_UUID)) {
                        BluetoothGattCharacteristic characteristic =
                                service.getCharacteristic(UUID.fromString(PAYLOAD_CHARACTERISTIC_UUID));

                        DATA_PIPELINE_PROGRESS = "Service was discovered";
                        gatt.readCharacteristic(characteristic);
                    }
                }
            }

            @Override
            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                if(status != BluetoothGatt.GATT_SUCCESS){
                    Sentry.capture("Failed to write characteristic with status " + status);
                }
                super.onCharacteristicWrite(gatt, characteristic, status);
            }

            @Override
            public void onCharacteristicRead(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, int status) {
                Measurement newMeasurement = new Measurement();
                newMeasurement.payload = bytesToHex(characteristic.getValue());

                if (lastKnownLocation != null) {
                    newMeasurement.accuracy = lastKnownLocation.getAccuracy();
                    newMeasurement.latitude = lastKnownLocation.getLatitude();
                    newMeasurement.longitude = lastKnownLocation.getLongitude();
                }

                SharedPreferences mSettings = context.getSharedPreferences("Session", 0);
                if (mSettings.contains("adrich.ignoreMeasurement")) {
                    String ignoreAsString = mSettings.getString("adrich.ignoreMeasurement", null);
                    if (ignoreAsString != null && ignoreAsString.equals(("true"))) {
                        newMeasurement.ignore = true;
                    }
                }

                Gson gson = new Gson();
                String lastMeasurementJson = gson.toJson(newMeasurement);

                EventEmitter.sendEvent("ADRICH_PAYLOAD_TRANSMITTED");
                DATA_PIPELINE_PROGRESS = "Transmission of payload was attempted";

                SendMeasurementThread sender = new SendMeasurementThread(lastMeasurementJson, context);
                sender.start();

                try {
                    newMeasurement.save(context);

                    characteristic.setValue("ADRICH");
                    if (!gatt.writeCharacteristic(characteristic)) {
                        Sentry.capture(new Exception("gatt.writeCharacteristic returned false."));
                    }
                } catch (Exception e) {
                    Sentry.capture(e);
                }
            }
        };
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        isActive = false;
    }
}
