package com.adrichmobile;

public final class FCMToken {

    public FCMToken(String token) {
       this.fcmToken = token;
    }

    public String fcmToken;

    @Override
    public String toString() {
        return "fcmToken: " + this.fcmToken;
    }
}