package com.adrichmobile;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.SystemClock;

import com.facebook.react.ReactApplication;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.chirag.RNMail.RNMail;
import ca.jaysoo.extradimensions.ExtraDimensionsPackage;

import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;
import io.sentry.event.UserBuilder;

import com.psykar.cookiemanager.CookieManagerPackage;
import com.solinor.bluetoothstatus.RNBluetoothManagerPackage;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {
    public static boolean PROHIBITIVE_APP_PERMISSIONS_HAVE_BEEN_UPDATED = false;

    private Handler checkPermissionsHandler = null;
    private Handler checkUserHandler = null;

    private Runnable checkPermissionsRunnable = new Runnable() {
        @Override
        public void run() {
        if (PROHIBITIVE_APP_PERMISSIONS_HAVE_BEEN_UPDATED) {
            Sentry.capture("Location services have been updated. Starting runnable.");
            startBackgroundService();
            checkPermissionsHandler.removeCallbacks(checkPermissionsRunnable);
        } else {
            checkPermissionsHandler.postDelayed(checkPermissionsRunnable, 2500);
        }
        }
    };

    private Runnable checkUserRunnable = new Runnable() {
        @Override
        public void run() {
        SharedPreferences mSettings = getApplicationContext().getSharedPreferences("Session", 0);
        if (mSettings.contains("email")) {
            Sentry.getContext().setUser(
                new UserBuilder().setEmail(mSettings.getString("email", null)).build()
            );
            checkUserHandler.removeCallbacks(checkUserRunnable);
        } else {
            checkUserHandler.postDelayed(checkUserRunnable, 5000);
        }
        }
    };

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
            new RNDeviceInfo(),
            new RNMail(),
                    new ExtraDimensionsPackage(),
                    new RNBluetoothManagerPackage(),
                    new CookieManagerPackage(),
                    new ReactNativeConfigPackage(),
                    new AdrichMobilePackage()
            );
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    private void startBackgroundService() {
        if (!DataCollectionService.isActive) {
            Intent intent = new Intent(getApplicationContext(), DataCollectionService.class);
            startService(intent);
        }
    }

    @Override
    public void onCreate() {
        DatabaseHelper.getInstance(getApplicationContext());

        // Start the scanner once we have the correct permissions.
        checkPermissionsHandler = new Handler();
        checkPermissionsHandler.postDelayed(checkPermissionsRunnable, 2000);

        // Init Sentry
        Context ctx = getApplicationContext();
        String SENTRY_DSN = "https://e08bd4f5b6c34972a0f0b946fac8e5f0:40c37cb4bb2a4ca49c559bb624540953@sentry.io/1235816";
        String SENTRY_DSN_WITH_OPTIONS = SENTRY_DSN + "?environment=" + BuildConfig.API_SERVER;
        Sentry.init(SENTRY_DSN_WITH_OPTIONS, new AndroidSentryClientFactory(ctx));

        // Look for a user until we see one
        checkUserHandler = new Handler();
        checkUserHandler.postDelayed(checkUserRunnable, 2000);

        Intent syncIntent = new Intent(this, DataSynchronizer.class);
        PendingIntent pendingSyncIntent = PendingIntent.getBroadcast(this.getApplicationContext(),
                234324243, syncIntent, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        // NOTE: If there is already an alarm scheduled for the same IntentSender,
        // that previous alarm will first be canceled.
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_HOUR / 6,
                AlarmManager.INTERVAL_HOUR / 6, pendingSyncIntent);

        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
