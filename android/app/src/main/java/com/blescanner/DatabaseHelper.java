package com.adrichmobile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class DatabaseHelper extends SQLiteOpenHelper {

    // It is recommended to use the Singleton pattern to avoid memory leaks
    // and unnecessary reallocations.
    private static DatabaseHelper instance;

    //  /data/data/com.adrichreactnative/databases/adrich.db
    public static final String DATABASE_NAME = "adrich000.db";
    public static final int DATABASE_VERSION = 1;

    public static String getFullDatabasePath() {
        return "/data/data/com.adrichmobile/databases/" + DATABASE_NAME;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }

        // forces onCreate to be called.
        instance.getWritableDatabase();

        return instance;
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS Measurement(" +
                        "id INTEGER PRIMARY KEY, " +
                        "data TEXT" +
                        ");"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS SentMeasurement(" +
                        "id INTEGER PRIMARY KEY, " +
                        "data TEXT" +
                        ");"
        );

        db.execSQL(
                "CREATE TABLE IF NOT EXISTS Session(" +
                        "id INTEGER PRIMARY KEY, " +
                        "value TEXT" +
                        ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO use this when changes occur to the db.
    }

    public String getSessionValue() {
        SQLiteDatabase db = getReadableDatabase();

        long rowCount = DatabaseUtils.queryNumEntries(db, "Session");

        if (rowCount > 0) {
            String cookieValue = null;

            try {
                Cursor cursor = db.rawQuery("select * from Session",null);

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        cookieValue = cursor.getString(1);
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
                Log.d("sessionnnnn", e.toString());
            }

            return cookieValue;
        }

        return null;
    }

    public List<String> getMeasurements(final String tablename) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from " + tablename,null);
        ArrayList<String> list = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                int dataIndex = cursor.getColumnIndexOrThrow("data");
                list.add(cursor.getString(dataIndex));
            } while (cursor.moveToNext());
        }

        return list;
    }

    public void addMeasurement(String table, String measurementAsJson) {
        SQLiteDatabase db = getWritableDatabase();

        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put("data", measurementAsJson);

            db.insertOrThrow("Measurement", null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public String getMeasurementsAsJson() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from Measurement",null);
        ArrayList<String> list = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                int dataIndex = cursor.getColumnIndexOrThrow("data");
                list.add(cursor.getString(dataIndex));
            } while (cursor.moveToNext());
        }

        if (list.size() == 0) {
            return "[]";
        }

        String response = "[";
        for (int i = 0; i < list.size(); i++) {
            response += list.get(i);

            if (i != list.size() - 1) {
                response += ",";
            }
        }
        response += "]";

        return response;
    }
}