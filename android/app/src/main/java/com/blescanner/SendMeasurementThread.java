package com.adrichmobile;

import android.content.ContentValues;
import android.content.Context;
//import android.content.SharedPreferences;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;

class SendMeasurementThread extends Thread {

    private String measurementAsJson;
    private Context context;

    public SendMeasurementThread(String measurementAsJson, Context context) {
        this.measurementAsJson = measurementAsJson;
        this.context = context;
    }

    private void markMeasurementAsSent() {
        SQLiteDatabase db = SQLiteDatabase.openDatabase(DatabaseHelper.getFullDatabasePath(),
                null, SQLiteDatabase.OPEN_READWRITE);

        try {
            db.beginTransaction();
            
            ContentValues values = new ContentValues();
            values.put("data", measurementAsJson);

            db.insertOrThrow("SentMeasurement", null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }

    }

    public void run() {

        try {

            URL url = new URL(BuildConfig.API_SERVER + "/api/measurements");
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();

            uc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

            // Set cookie if it exists
            SharedPreferences mSettings = context.getSharedPreferences("Session", 0);
            if (mSettings.contains("id")) {
                String sessionString= mSettings.getString("id", null);
                String myCookie = "adrich.session=" + sessionString;
                uc.setRequestProperty("Cookie", myCookie);
            }

            uc.setRequestMethod("POST");
            uc.setDoInput(true);
            uc.setInstanceFollowRedirects(false);
            uc.connect();

            OutputStreamWriter writer = new OutputStreamWriter(uc.getOutputStream(), "UTF-8");
            writer.write(measurementAsJson);
            writer.close();

            int responseCode = uc.getResponseCode();
            uc.disconnect();

            if (responseCode < 400) {
                EventEmitter.sendEvent("ADRICH_PAYLOAD_RESOLVED");
                DataCollectionService.DATA_PIPELINE_PROGRESS = null;
                this.markMeasurementAsSent();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}