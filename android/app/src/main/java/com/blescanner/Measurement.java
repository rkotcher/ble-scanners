package com.adrichmobile;

import android.content.Context;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class Measurement {

    public Measurement() {
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        String version = BuildConfig.VERSION_NAME;
        int code = BuildConfig.VERSION_CODE;

        this.buildVersion = version + " (" + code + ")";
        this.localdatetime = format.format(now);
        this.mobileReceivedAt = System.currentTimeMillis();
        this.zoneinfo = TimeZone.getDefault().getID();
    }

    public String client = "android";
    public boolean ignore = false;

    public String buildVersion;
    public String localdatetime;
    public long mobileReceivedAt;
    public String zoneinfo;
    public String payload;
    public Float accuracy;
    public Double latitude;
    public Double longitude;

    public boolean save(Context context) {
        try {
            DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
            Gson gson = new Gson();
            databaseHelper.addMeasurement("Measurement", gson.toJson(this));
            return true;
        } catch (Exception e) {
            return true;
        }
    }

    @Override
    public String toString() {
        String value = "";

        value +=
            "client: " + client + "\n" +
            "buildVersion: " + buildVersion + "\n" +
            "payload : " + payload + "\n" +
            "localdatetime: " + localdatetime + "\n" +
            "ignore: " + ignore + "\n" +
            "zoneinfo: " + zoneinfo + "\n" +
            "accuracy: " + accuracy + "\n" +
            "latitude: " + latitude + "\n" +
            "longitude: " + longitude + "\n";

        return value;
    }
}