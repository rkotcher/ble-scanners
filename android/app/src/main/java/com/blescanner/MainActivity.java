package com.adrichmobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.react.ReactActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import io.sentry.Sentry;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "adrichmobile";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Task<InstanceIdResult> task= FirebaseInstanceId.getInstance().getInstanceId();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(
                MainActivity.this,  new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String newToken = instanceIdResult.getToken();
                        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = mSettings.edit();
                        editor.putString("fcm", newToken);
                        editor.commit();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Sentry.capture(e);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!DataCollectionService.isActive) {
            Intent intent = new Intent(getApplicationContext(), DataCollectionService.class);
            startService(intent);
        }

        // NOTE: I'm not sure how else to get the context, but there is probably
        // a better way.
        EventEmitter.reactInstanceManager = getReactInstanceManager();
    }
}
