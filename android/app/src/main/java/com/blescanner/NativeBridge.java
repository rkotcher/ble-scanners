package com.adrichmobile;

import android.content.SharedPreferences;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class NativeBridge extends ReactContextBaseJavaModule {
    ReactApplicationContext context;

    public NativeBridge(ReactApplicationContext reactContext) {
        super(reactContext);
        context = reactContext;
    }

    @Override
    public String getName() { return "NativeBridge"; }

    @ReactMethod
    public void setPermissionsHaveBeenUpdated() {
        MainApplication.PROHIBITIVE_APP_PERMISSIONS_HAVE_BEEN_UPDATED = true;
    }

    @ReactMethod
    public void setSession(String session) {
        SharedPreferences mSettings = context.getSharedPreferences("Session", 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("id", session);
        editor.apply();

        SendFCMToken fcm = new SendFCMToken(context);
        fcm.start();
    }

    @ReactMethod
    public void setIgnoreMeasurement() {
        SharedPreferences mSettings = context.getSharedPreferences("Session", 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("adrich.ignoreMeasurement", "true");
        editor.apply();
    }

    @ReactMethod
    public void unsetIgnoreMeasurement() {
        SharedPreferences mSettings = context.getSharedPreferences("Session", 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("adrich.ignoreMeasurement", "false");
        editor.apply();
    }

    @ReactMethod
    public void setEmail(String email) {
        SharedPreferences mSettings = context.getSharedPreferences("Session", 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("email", email);
        editor.apply();
    }

    @ReactMethod
    public void clearSession() {
        SharedPreferences mSettings = context.getSharedPreferences("Session", 0);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.clear();
        editor.apply();
    }
}