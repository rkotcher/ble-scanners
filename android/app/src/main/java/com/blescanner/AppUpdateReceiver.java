package com.adrichmobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import io.sentry.Sentry;

public class AppUpdateReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Sentry.capture("Update received.");
        Intent service = new Intent(context, DataCollectionService.class);
        context.startService( service );
    }
}