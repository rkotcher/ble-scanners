package com.adrichmobile;

import android.util.EventLog;
import android.util.Log;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class EventEmitter {
    public static ReactInstanceManager reactInstanceManager;

    public static void sendEvent(String name) {
        try {
            ReactContext context = reactInstanceManager.getCurrentReactContext();
            if (context != null) {
                WritableMap params = Arguments.createMap();
                params.putString("name", name);

                context
                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                    .emit("PayloadLifecycleEventOccurred", params);
            }
        } catch(Exception e) {
            /* TODO */
        }
    }
}
