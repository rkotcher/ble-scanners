package com.adrichmobile;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

public class DataSynchronizer extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(context);
        ArrayList<String> measurementData =
                (ArrayList<String>) databaseHelper.getMeasurements("Measurement");

        ArrayList<String> sentMeasurementData =
                (ArrayList<String>) databaseHelper.getMeasurements("SentMeasurement");

        for (String measurementAsJson : measurementData) {
            if (!sentMeasurementData.contains(measurementAsJson)) {
                SendMeasurementThread sender = new SendMeasurementThread(measurementAsJson, context);
                sender.start();
                break;
            }
        }
    }
}
