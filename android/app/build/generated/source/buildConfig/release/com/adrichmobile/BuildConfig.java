/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.adrichmobile;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.adrichmobile";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 93;
  public static final String VERSION_NAME = "0.12.0";
  // Fields from default config.
  public static final String API_SERVER = "https://data.adrich.io";
}
