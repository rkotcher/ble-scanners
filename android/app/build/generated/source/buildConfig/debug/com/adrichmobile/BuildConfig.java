/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.adrichmobile;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.adrichmobile";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 91;
  public static final String VERSION_NAME = "0.11.4-bissell-iteration";
  // Fields from default config.
  public static final String API_SERVER = "http://dev.adrich.io";
}
